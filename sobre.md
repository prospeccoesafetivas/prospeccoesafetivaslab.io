---
title: Sobre
layout: default
---

<div class="container-fluid">
  <div class="row">
    <div class="sobre col-sm-10">
      <h5>
        O que afetamos e o que nos afeta?
        É na relação de forças entre arte e sociedade, numa conjunção e  acoplamento entre signo e poder, que se combinam com uma filosofia da resistência pelo afeto. A proposta dessa exposição são afeições que surgem nas diferentes propostas artísticas, muitas das quais percebidas nas sensações midiatizadas que estão disponíveis na atualidade, mas também na passagem pelas percepções sensoriais de cada obra exposta aqui. <br>
        A curadoria buscou atrair forças ao invés de reproduzir e inventar formas de arte. Essa proposta, também, se distancia do assédio que continuamos recebendo das mídias digitais interativas, para que continuemos consumindo, para salvar o capitalismo tardio. Nossas casas não são mais somente espaços de convivência familiar.<br> Estamos vivendo um novo ciberespaço, além do que já foi imaginado pela ficção científica e que encontramos na literatura. No re-design de um novo espaço de colaboração, no qual podemos nos encontrar para compartilhar ideias com arte, design e tecnologia é que a exposição intitulada Prospecções Afetivas apresenta aventuras insurgidas em tempos de pandemia.
      </h5>
      <p class='nome-autor'>Suzete Venturelli , 2020</p>

  <h4>What do we affect and what affects us? It is in the relation of forces between art and society, in a conjunction and coupling between sign and power that a philosophy of resistance by affection is combined. The purpose of this exhibition are affections that appear in different artistic proposals, many of which are perceived in the mediatized sensations that are available today, but also in the passage through the sensory perceptions of each work exposed here.<br>
  The curatorship sought to attract forces instead of reproducing and inventing art forms. This proposal, too, moves away from the harassment that we continue to receive from interactive digital media, so that we continue to consume to save late capitalism. Our homes are no longer just spaces for family living.<br>
  We are living in a new cyberspace, beyond what was already imagined by science fiction and found in literature. In the re-design of a new collaboration space, in which we can meet to share ideas of art, design and technology, the exhibition entitled Affective Prospecting presents adventures that arose in times of pandemic.
      </h4>
      <p class='nome-autor'>Suzete Venturelli , 2020</p>
    </div>
  </div>
</div>
