// The generative fingerprint is an instance of this JavaScript [class](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes) I made called "FP", it's worth noting that the original name was "FingerPrint" but it didn't work on Firefox. Though a bit more testing would be needed to confirm this, it seems Firefox is blocking code simply named "FingerPrint", an unconventional (and refreshingly aggresive) approach to protecting their user's privacy.

/*
Creating an instance of FP will imediately create and append the fingerprint to
the body of your HTML page:

    const fp = new FP()

You can optionally pass as the first argument in the constructor the CSS selector
of the particular element you'd like to render the fingerprint. There's also an
optional options object for displaying error's in alert boxes for debugging mobile.

    const fp = new FP('#targetElement',{mobileDebug:true})
*/

class FP {
    constructor(parentSlector,params){
        this.svgNS = "http://www.w3.org/2000/svg"
        this.svgBox = document.createElementNS(this.svgNS, "svg")
        this.svgBox.setAttributeNS(null,"viewBox","0 0 660 656")
        this.svgBox.setAttributeNS(null,"id","fingerPrint")
        this.svgBox.setAttributeNS(null,"fill","#fff")
        this.svgBox.style.fontFamily = 'monospace'
        this.svgBox.style.fontSize = '12px'
        this.svgBox.style.fontWeight = 'bold'
        this.svgBox.style.width = '70%'
        this.svgBox.style.display = 'block'
        this.svgBox.style.margin = '0 auto'

        if(parentSlector){
            if(typeof parentSlector=="string"){
                document.querySelector(parentSlector).appendChild(this.svgBox)
            } else {
                throw new Error('FP constructor needs a querySelector string')
            }
        } else {
            document.body.appendChild(this.svgBox)
        }

        if(params){
            if(params.mobileDebug){
                window.onerror = function(msg, url, linenumber) {
                    alert('Error message: '+msg+'\nURL: '+url+'\nLine Number: '+linenumber);
                    return true;
                }
            }
        }

        this.fontList = this.getFonts()
        this.canv  = this.getCanvasFingerPrint()
        if(window.crypto || window.msCrypto){
            this.hashIt(this.canv,(hash)=>{
                this.canv = hash
                this.all = this.updateAll()
            })
        }

        this.all = this.updateAll()
        this.lines = []
        this.curvePoints.forEach((curve,i)=>{
            let idx = i*40
            let str = this.allOffset(idx)
            let l = this.curveText(curve,i,str)
            this.lines.push({element:l,index:idx})
        })

        this.animate()
    }

    //--------------------------------------------------------------------------

    // The methods below are used to dynamically create and animate the SVG elements whose textContent will be all the uniqe browser info which makes up the fingerprint. SVG animations are cool and all, but the purpose of this documentation is to annotate the [differnet methods](#section-7) used to detect the individual bits of browser info used in creating the identifiable hash, so I won't spend much time explaining this part. That said, there's plenty of info online about SVG animations.

    get curvePoints(){
        return [
            'M 67, 398 C 44, 350, 62, 221, 95, 202',
            'M 107, 172 C 179, 54, 359, 30, 455, 86',
            'M 485, 104 C 552, 145, 613, 256, 604, 339',
            'M 99, 398 C 57, 222, 191, 66, 367, 94',
            'M 400, 102 C 511, 136, 601, 247, 573, 461',
            'M 93, 472 C 132, 414, 141, 414, 125, 333',
            'M 128, 299 C 148, 100, 479, 19, 538, 322',
            'M 541, 354 C 545, 429, 538, 468, 523, 526',
            'M 117, 498 C 233, 373, 82, 329, 226, 195',
            'M 260, 175 C 305, 149, 394, 152, 442, 200 ',
            'M 468, 227 C 514, 286, 524, 428, 476, 563',
            'M 135, 528 C 162, 501, 179, 476, 191, 448',
            'M 201, 411 C 133, 115, 548, 108, 466, 465',
            'M 460, 503 C 456, 519, 442, 568, 428, 588',
            'M 161, 547 C 318, 419, 125, 268, 342, 227',
            'M 377, 239 C 435, 256, 482, 411, 381, 601',
            'M 191, 567 C 247, 518, 281, 414, 266, 357',
            'M 262, 321 C 296, 222, 417, 249, 404, 393',
            'M 404, 431 C 403, 480, 368, 569, 338, 606',
            'M 223, 586 C 244, 561, 279, 511, 286, 484',
            'M 297, 451 C 315, 363, 286, 343, 304, 306',
            'M 326, 296 C 386, 285, 399, 461, 297, 604',
            'M 257, 597 C 345, 490, 341, 410, 331, 326'
        ]
    }

    curveText(points,i,string){
        let xlinkNS = 'http://www.w3.org/1999/xlink'
        let path = document.createElementNS(this.svgNS,"path")
            path.setAttributeNS(null,"id",`curve${i}`)
            path.setAttributeNS(null,"d",points)
            path.style.fill = "transparent"
        this.svgBox.appendChild(path)
        let txt = document.createElementNS(this.svgNS,"text")
            txt.setAttributeNS(null,'x',25)
        let txtPath = document.createElementNS(this.svgNS,"textPath")
            txtPath.setAttributeNS(xlinkNS,"xlink:href",`#curve${i}`)
            txtPath.textContent = string
        txt.appendChild(txtPath)
        this.svgBox.appendChild(txt)
        return txtPath
    }

    allOffset(n){
        let str = this.all.substr(n,100)
        if(str.length < 100){
            str += ' :: ' + this.all.substr(0,100-str.length)
        }
        return str
    }

    animate(){
        setTimeout(()=>{
            this.animate()
        },100)

        this.lines.forEach(l=>{
            l.index++
            if(l.index > this.all.length) l.index = 0
            l.element.textContent = this.allOffset(l.index)
        })
    }

    //--------------------------------------------------------------------------

    // As mentioned in the article, you can test this yourself in your JavaScript Console. If you’re using Firefox, use the shortcut key Control+Shift+K (or Command+Shift+K on Mac). If you’re using Chrome use Control+Shift+J (or Command+Shift+J on Mac) and this will display a window with a command line interface. Once open, type [`navigator.userAgent`](https://developer.mozilla.org/en-US/docs/Web/API/NavigatorID/userAgent) and hit enter. You should see your device’s “user-agent” print to that console. The “[user-agent](https://webaim.org/blog/user-agent-string-history/)” is used to identify what browser and platform you are using.

    // Other bits of browser info typically used in fingerprinting are [`navigator.language`](https://developer.mozilla.org/en-US/docs/Web/API/NavigatorLanguage/language), [`navigator.doNotTrack`](https://developer.mozilla.org/en-US/docs/Web/API/Navigator/doNotTrack) and  [`navigator.plugins`](https://developer.mozilla.org/en-US/docs/Web/API/NavigatorPlugins/plugins) the canvas is also refenced here, but the actual code used to fingerprint the canvas is in the [getCanvasFingerPrint](#section-17) below.

    get userAgent(){ return `user-agent:${navigator.userAgent}` }
    get language(){ return `language:${navigator.language}` }
    get canvas(){ return `canvas-hash:${this.canv}`}
    get doNotTrack(){
        let dnt = navigator.doNotTrack
        if(dnt==1) dnt = "enabled"
        else if(dnt==0) dnt = "disabled"
        return `do-not-track:${dnt}`
    }
    get plugins(){
        let str = ''
        for(let i=0; i<navigator.plugins.length; i++){
            str +=  `plugin-${i+1}:${navigator.plugins[i].name}; `
                +`${navigator.plugins[i].description}; `
                +`${navigator.plugins[i].filename} `
            for (let j = 0; j < navigator.plugins[i].length; j++) {
                str += `(${navigator.plugins[i][j].description}; `
                    +`${navigator.plugins[i][j].type}; `
                    +`${navigator.plugins[i][j].suffixes}) `
            }
        }
        return str
    }

    // Other browser APIs give you access to device information, for example the `screen` object gives you access to the  [`screen.width`](https://developer.mozilla.org/en-US/docs/Web/API/Screen/width), the [`screen.height`](https://developer.mozilla.org/en-US/docs/Web/API/Screen/height) and the [`screen.colorDepth`](https://developer.mozilla.org/en-US/docs/Web/API/Screen/colorDepth). The `navigator` object also gives you access to some device information, like  [`navigator.platform`](https://developer.mozilla.org/en-US/docs/Web/API/NavigatorID/platform) which specifies the platform the users browser was compiled for as well as [`navigator.hardwareConcurrency`](https://developer.mozilla.org/en-US/docs/Web/API/NavigatorConcurrentHardware/hardwareConcurrency) which tells you the number of logical processors on the user's machine.
    get resolution(){ return `resolution:${screen.width}x${screen.height}` }
    get colorDepth(){ return `color-depth:${screen.colorDepth}` }
    get timeZone(){
        return `timezone:${
            Intl.DateTimeFormat().resolvedOptions().timeZone ? Intl.DateTimeFormat().resolvedOptions().timeZone
            : new Date().getTimezoneOffset()
        }`
    }
    get platform(){ return `platform:${navigator.platform}`}
    get fonts(){ return `font-list:${this.fontList}`}
    get cpu(){ return `CPU-cores:${navigator.hardwareConcurrency}`}
    // [WebGL](https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API) an API for genreating 3D graphics in the browser can be used to uncover information about the graphics card installed on your device.
    get gpu(){
        const canvas = document.createElement('canvas')
        const gl = canvas.getContext('webgl')
        let dbgRenNfo = gl.getExtension("WEBGL_debug_renderer_info")
        let vendor = gl.getParameter(gl.VENDOR)
        let renderer = gl.getParameter(gl.RENDERER)
        if(dbgRenNfo){
            vendor = gl.getParameter(dbgRenNfo.UNMASKED_VENDOR_WEBGL)
            renderer = gl.getParameter(dbgRenNfo.UNMASKED_RENDERER_WEBGL)
        }
        return `GPU-vendor:${vendor}; GPU-renderer:${renderer}`
    }

    // As explained in [this post](https://stackoverflow.com/a/4819886/1104148) it's possible to detect whether or not the user has a touch screen, this provides an additional bit of identifiable information pertaining to the user's device
    get touch(){
        let prefixes = ' -webkit- -moz- -o- -ms- '.split(' ')
        let mq = function(query){ return window.matchMedia(query).matches }
        if (('ontouchstart' in window) ||
            window.DocumentTouch && document instanceof DocumentTouch) {
                return `touch-device:Yes`
        }
        let query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')']
            query = query.join('')
        if( mq(query) ) return `touch-device:Yes`
        else return `touch-device:No`
    }

    // For security reasons, browsers do not give webpages access to your device's file system. But because web apps require the ability to occasionlly save information, there are a few browser API's available for storing user data, these include [localStorage](https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage), [sessionStorage](https://developer.mozilla.org/en-US/docs/Web/API/Window/sessionStorage) and [indexedDB](https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/indexedDB). Because users have started blocking cookies, trackers have started to use these APIs to store the same data they would otherwise store in a cookie. This use of storage APIs for storing information used for tracking purposes is known as "super cookies"
    get localStorage(){
        if(window.localStorage) return `local-storage:Yes`
        else return `local-storage:No`
    }
    get sessionStorage(){
        if(window.sessionStorage) return `session-storage:Yes`
        else return `session-storage:No`
    }
    get indexedDB(){
        if(window.indexedDB) return `indexedDB-storage:Yes`
        else return `indexedDB-storage:No`
    }

    updateAll(){
        return ''
        + ' :: ' + this.userAgent
        + ' :: ' + this.language
        + ' :: ' + this.doNotTrack
        + ' :: ' + this.plugins
        + ' :: ' + this.resolution
        + ' :: ' + this.colorDepth
        + ' :: ' + this.timeZone
        + ' :: ' + this.platform
        + ' :: ' + this.cpu
        + ' :: ' + this.gpu
        + ' :: ' + this.touch
        + ' :: ' + this.localStorage
        + ' :: ' + this.sessionStorage
        + ' :: ' + this.indexedDB
        + ' :: ' + this.canvas
        + ' :: ' + this.fonts
    }

    //--------------------------------------------------------------------------
    // These are a couple methods used for creating a hash (unique id) of the browser's canvas. On it's own, the browser's canvas fingerprint (created by the [getCanvasFingerPrint](#section-17) method below) if far too long to digest. As mentioned in the article, although this animated fingerprint reveals the individual pieces of information typically used in browser fingerprinting in an aesthetic way, when actually used for tracking it's typical to compress all the information into a single ID using a similar cryptographic hash function as this.
    str2ArrayBuffer(str) {
        let bytes = new Uint8Array(str.length)
        for (let i = 0; i < str.length; i++){
            bytes[i] = str.charCodeAt(i)
        }
        return bytes
    }
    arrayBuffer2Hex(buf){
        let data_view = new DataView(buf)
        let i, len, hex = '', c
        for(i=0, len=data_view.byteLength; i<len; i++){
            c = data_view.getUint8(i).toString(16)
            if(c.length < 2) c = '0'+c
            hex += c
        }
        return hex
    }
    hashIt(data,callback){
        let crypto = window.crypto || window.msCrypto
        let opts = {name:'SHA-256'}
        let promise
        if(crypto.webkitSubtle){
            promise = crypto.webkitSubtle.digest(opts,this.str2ArrayBuffer(data))
        } else if(crypto.subtle){
            promise = crypto.subtle.digest(opts,this.str2ArrayBuffer(data))
        } else {
            return callback(data)
        }

        promise.then((res)=>{
            let hash_value = this.arrayBuffer2Hex( res )
            callback(hash_value)
        })
    }

    // -------------------------------------------------------------------------

    // Canvas fingerprinting is perhaps one of the most clever ways of deriving identifiable data on a user's device, so much so it has it's own [wikipedia page](https://en.wikipedia.org/wiki/Canvas_fingerprinting). In order to derive a canvas fingerprint you create a [canvas element](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API) (typically used for 2D or 3D [animations](https://www.html5canvastutorials.com/)) and draw some text with some overlayed colors. Because of variations in the GPU and graphics drivers of different devices the images rendered to the canvas, though similar to our eyes, won't be exactly the same pixel by pixel. We can generate a 'data url' (a string/text represation of the pixels in the canvas) which functions as our canvas fingerprint.
    getCanvasFingerPrint(){
        const canvas = document.createElement('canvas')
        const ctx = canvas.getContext('2d')
        const strz = [
          "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKL",
          "MNOPQRSTUVWXYZ`~1!2@3#4$5%6^7&8*9",
          "(0)-_=+[{]}|;:',<.>/?"
        ]

        ctx.textBaseline = "top"
        ctx.font = "14px 'Arial'"
        ctx.textBaseline = "alphabetic"
        ctx.fillStyle = "#f60"
        ctx.fillRect(20, 1, 62, 100)

        strz.forEach( (str,i) => {
            ctx.fillStyle = "#069"
            ctx.fillText(str, 2, 18*(i+1))
            ctx.fillStyle = "rgba(102, 204, 0, 0.7)"
            ctx.fillText(str, 4, 20*(i+1))
        })

        ctx.globalCompositeOperation = 'multiply'
        ctx.fillStyle = "#f60"
        ctx.fillRect(200, 1, 62, 100)

        return canvas.toDataURL()
    }

    // Another clever trick used to derive identifiable information is creating a list of installed fonts. Browsers need access to your fonts in order to render the texts on your screen, but because users often add to the list of fonts that come default on their devices, this can become a particularly effective way to identify you online. There are different ways to do this (including using Flash). The method I'm using here is modified version of a method created by [Lalit Patel](https://stackoverflow.com/a/3368855/1104148)
    getFonts(){
        const baseFonts = ['monospace', 'sans-serif', 'serif']

        const fontList = [
         'Andale Mono', 'Arial', 'Arial Black', 'Arial Hebrew', 'Arial MT', 'Arial Narrow', 'Arial Rounded MT Bold', 'Arial Unicode MS',
         'Bitstream Vera Sans Mono', 'Book Antiqua', 'Bookman Old Style',
         'Calibri', 'Cambria', 'Cambria Math', 'Century', 'Century Gothic', 'Century Schoolbook', 'Comic Sans', 'Comic Sans MS', 'Consolas', 'Courier', 'Courier New',
         'Geneva', 'Georgia',
         'Helvetica', 'Helvetica Neue',
         'Impact',
         'Lucida Bright', 'Lucida Calligraphy', 'Lucida Console', 'Lucida Fax', 'LUCIDA GRANDE', 'Lucida Handwriting', 'Lucida Sans', 'Lucida Sans Typewriter', 'Lucida Sans Unicode',
         'Microsoft Sans Serif', 'Monaco', 'Monotype Corsiva', 'MS Gothic', 'MS Outlook', 'MS PGothic', 'MS Reference Sans Serif', 'MS Sans Serif', 'MS Serif', 'MYRIAD', 'MYRIAD PRO',
         'Palatino', 'Palatino Linotype',
         'Segoe Print', 'Segoe Script', 'Segoe UI', 'Segoe UI Light', 'Segoe UI Semibold', 'Segoe UI Symbol',
         'Tahoma', 'Times', 'Times New Roman', 'Times New Roman PS', 'Trebuchet MS',
         'Verdana', 'Wingdings', 'Wingdings 2', 'Wingdings 3',
         'Abadi MT Condensed Light', 'Academy Engraved LET', 'ADOBE CASLON PRO', 'Adobe Garamond', 'ADOBE GARAMOND PRO', 'Agency FB', 'Aharoni', 'Albertus Extra Bold', 'Albertus Medium', 'Algerian', 'Amazone BT', 'American Typewriter',
         'American Typewriter Condensed', 'AmerType Md BT', 'Andalus', 'Angsana New', 'AngsanaUPC', 'Antique Olive', 'Aparajita', 'Apple Chancery', 'Apple Color Emoji', 'Apple SD Gothic Neo', 'Arabic Typesetting', 'ARCHER',
         'ARNO PRO', 'Arrus BT', 'Aurora Cn BT', 'AvantGarde Bk BT', 'AvantGarde Md BT', 'AVENIR', 'Ayuthaya', 'Bandy', 'Bangla Sangam MN', 'Bank Gothic', 'BankGothic Md BT', 'Baskerville',
         'Baskerville Old Face', 'Batang', 'BatangChe', 'Bauer Bodoni', 'Bauhaus 93', 'Bazooka', 'Bell MT', 'Bembo', 'Benguiat Bk BT', 'Berlin Sans FB', 'Berlin Sans FB Demi', 'Bernard MT Condensed', 'BernhardFashion BT', 'BernhardMod BT', 'Big Caslon', 'BinnerD',
         'Blackadder ITC', 'BlairMdITC TT', 'Bodoni 72', 'Bodoni 72 Oldstyle', 'Bodoni 72 Smallcaps', 'Bodoni MT', 'Bodoni MT Black', 'Bodoni MT Condensed', 'Bodoni MT Poster Compressed',
         'Bookshelf Symbol 7', 'Boulder', 'Bradley Hand', 'Bradley Hand ITC', 'Bremen Bd BT', 'Britannic Bold', 'Broadway', 'Browallia New', 'BrowalliaUPC', 'Brush Script MT', 'Californian FB', 'Calisto MT', 'Calligrapher', 'Candara',
         'CaslonOpnface BT', 'Castellar', 'Centaur', 'Cezanne', 'CG Omega', 'CG Times', 'Chalkboard', 'Chalkboard SE', 'Chalkduster', 'Charlesworth', 'Charter Bd BT', 'Charter BT', 'Chaucer',
         'ChelthmITC Bk BT', 'Chiller', 'Clarendon', 'Clarendon Condensed', 'CloisterBlack BT', 'Cochin', 'Colonna MT', 'Constantia', 'Cooper Black', 'Copperplate', 'Copperplate Gothic', 'Copperplate Gothic Bold',
         'Copperplate Gothic Light', 'CopperplGoth Bd BT', 'Corbel', 'Cordia New', 'CordiaUPC', 'Cornerstone', 'Coronet', 'Cuckoo', 'Curlz MT', 'DaunPenh', 'Dauphin', 'David', 'DB LCD Temp', 'DELICIOUS', 'Denmark',
         'DFKai-SB', 'Didot', 'DilleniaUPC', 'DIN', 'DokChampa', 'Dotum', 'DotumChe', 'Ebrima', 'Edwardian Script ITC', 'Elephant', 'English 111 Vivace BT', 'Engravers MT', 'EngraversGothic BT', 'Eras Bold ITC', 'Eras Demi ITC', 'Eras Light ITC', 'Eras Medium ITC',
         'EucrosiaUPC', 'Euphemia', 'Euphemia UCAS', 'EUROSTILE', 'Exotc350 Bd BT', 'FangSong', 'Felix Titling', 'Fixedsys', 'FONTIN', 'Footlight MT Light', 'Forte',
         'FrankRuehl', 'Fransiscan', 'Freefrm721 Blk BT', 'FreesiaUPC', 'Freestyle Script', 'French Script MT', 'FrnkGothITC Bk BT', 'Fruitger', 'FRUTIGER',
         'Futura', 'Futura Bk BT', 'Futura Lt BT', 'Futura Md BT', 'Futura ZBlk BT', 'FuturaBlack BT', 'Gabriola', 'Galliard BT', 'Gautami', 'Geeza Pro', 'Geometr231 BT', 'Geometr231 Hv BT', 'Geometr231 Lt BT', 'GeoSlab 703 Lt BT',
         'GeoSlab 703 XBd BT', 'Gigi', 'Gill Sans', 'Gill Sans MT', 'Gill Sans MT Condensed', 'Gill Sans MT Ext Condensed Bold', 'Gill Sans Ultra Bold', 'Gill Sans Ultra Bold Condensed', 'Gisha', 'Gloucester MT Extra Condensed', 'GOTHAM', 'GOTHAM BOLD',
         'Goudy Old Style', 'Goudy Stout', 'GoudyHandtooled BT', 'GoudyOLSt BT', 'Gujarati Sangam MN', 'Gulim', 'GulimChe', 'Gungsuh', 'GungsuhChe', 'Gurmukhi MN', 'Haettenschweiler', 'Harlow Solid Italic', 'Harrington', 'Heather', 'Heiti SC', 'Heiti TC', 'HELV',
         'Herald', 'High Tower Text', 'Hiragino Kaku Gothic ProN', 'Hiragino Mincho ProN', 'Hoefler Text', 'Humanst 521 Cn BT', 'Humanst521 BT', 'Humanst521 Lt BT', 'Imprint MT Shadow', 'Incised901 Bd BT', 'Incised901 BT',
         'Incised901 Lt BT', 'INCONSOLATA', 'Informal Roman', 'Informal011 BT', 'INTERSTATE', 'IrisUPC', 'Iskoola Pota', 'JasmineUPC', 'Jazz LET', 'Jenson', 'Jester', 'Jokerman', 'Juice ITC', 'Kabel Bk BT', 'Kabel Ult BT', 'Kailasa', 'KaiTi', 'Kalinga', 'Kannada Sangam MN',
         'Kartika', 'Kaufmann Bd BT', 'Kaufmann BT', 'Khmer UI', 'KodchiangUPC', 'Kokila', 'Korinna BT', 'Kristen ITC', 'Krungthep', 'Kunstler Script', 'Lao UI', 'Latha', 'Leelawadee', 'Letter Gothic', 'Levenim MT', 'LilyUPC', 'Lithograph', 'Lithograph Light', 'Long Island',
         'Lydian BT', 'Magneto', 'Maiandra GD', 'Malayalam Sangam MN', 'Malgun Gothic',
         'Mangal', 'Marigold', 'Marion', 'Marker Felt', 'Market', 'Marlett', 'Matisse ITC', 'Matura MT Script Capitals', 'Meiryo', 'Meiryo UI', 'Microsoft Himalaya', 'Microsoft JhengHei', 'Microsoft New Tai Lue', 'Microsoft PhagsPa', 'Microsoft Tai Le',
         'Microsoft Uighur', 'Microsoft YaHei', 'Microsoft Yi Baiti', 'MingLiU', 'MingLiU_HKSCS', 'MingLiU_HKSCS-ExtB', 'MingLiU-ExtB', 'Minion', 'Minion Pro', 'Miriam', 'Miriam Fixed', 'Mistral', 'Modern', 'Modern No. 20', 'Mona Lisa Solid ITC TT', 'Mongolian Baiti',
         'MONO', 'MoolBoran', 'Mrs Eaves', 'MS LineDraw', 'MS Mincho', 'MS PMincho', 'MS Reference Specialty', 'MS UI Gothic', 'MT Extra', 'MUSEO', 'MV Boli',
         'Nadeem', 'Narkisim', 'NEVIS', 'News Gothic', 'News GothicMT', 'NewsGoth BT', 'Niagara Engraved', 'Niagara Solid', 'Noteworthy', 'NSimSun', 'Nyala', 'OCR A Extended', 'Old Century', 'Old English Text MT', 'Onyx', 'Onyx BT', 'OPTIMA', 'Oriya Sangam MN',
         'OSAKA', 'OzHandicraft BT', 'Palace Script MT', 'Papyrus', 'Parchment', 'Party LET', 'Pegasus', 'Perpetua', 'Perpetua Titling MT', 'PetitaBold', 'Pickwick', 'Plantagenet Cherokee', 'Playbill', 'PMingLiU', 'PMingLiU-ExtB',
         'Poor Richard', 'Poster', 'PosterBodoni BT', 'PRINCETOWN LET', 'Pristina', 'PTBarnum BT', 'Pythagoras', 'Raavi', 'Rage Italic', 'Ravie', 'Ribbon131 Bd BT', 'Rockwell', 'Rockwell Condensed', 'Rockwell Extra Bold', 'Rod', 'Roman', 'Sakkal Majalla',
         'Santa Fe LET', 'Savoye LET', 'Sceptre', 'Script', 'Script MT Bold', 'SCRIPTINA', 'Serifa', 'Serifa BT', 'Serifa Th BT', 'ShelleyVolante BT', 'Sherwood',
         'Shonar Bangla', 'Showcard Gothic', 'Shruti', 'Signboard', 'SILKSCREEN', 'SimHei', 'Simplified Arabic', 'Simplified Arabic Fixed', 'SimSun', 'SimSun-ExtB', 'Sinhala Sangam MN', 'Sketch Rockwell', 'Skia', 'Small Fonts', 'Snap ITC', 'Snell Roundhand', 'Socket',
         'Souvenir Lt BT', 'Staccato222 BT', 'Steamer', 'Stencil', 'Storybook', 'Styllo', 'Subway', 'Swis721 BlkEx BT', 'Swiss911 XCm BT', 'Sylfaen', 'Synchro LET', 'System', 'Tamil Sangam MN', 'Technical', 'Teletype', 'Telugu Sangam MN', 'Tempus Sans ITC',
         'Terminal', 'Thonburi', 'Traditional Arabic', 'Trajan', 'TRAJAN PRO', 'Tristan', 'Tubular', 'Tunga', 'Tw Cen MT', 'Tw Cen MT Condensed', 'Tw Cen MT Condensed Extra Bold',
         'TypoUpright BT', 'Unicorn', 'Univers', 'Univers CE 55 Medium', 'Univers Condensed', 'Utsaah', 'Vagabond', 'Vani', 'Vijaya', 'Viner Hand ITC', 'VisualUI', 'Vivaldi', 'Vladimir Script', 'Vrinda', 'Westminster', 'WHITNEY', 'Wide Latin',
         'ZapfEllipt BT', 'ZapfHumnst BT', 'ZapfHumnst Dm BT', 'Zapfino', 'Zurich BlkEx BT', 'Zurich Ex BT', 'ZWAdobeF']

        const testString = 'mmmmmmmmmmlli'
        const testSize = '72px'
        const h = document.getElementsByTagName('body')[0]
        const baseFontsDiv = document.createElement('div')
        const fontsDiv = document.createElement('div')
        const defaultWidth = {}
        const defaultHeight = {}

        const createSpan = function(){
            const s = document.createElement('span')
            s.style.position = 'absolute'
            s.style.left = '-9999px'
            s.style.fontSize = testSize
            s.style.fontStyle = 'normal'
            s.style.fontWeight = 'normal'
            s.style.letterSpacing = 'normal'
            s.style.lineBreak = 'auto'
            s.style.lineHeight = 'normal'
            s.style.textTransform = 'none'
            s.style.textAlign = 'left'
            s.style.textDecoration = 'none'
            s.style.textShadow = 'none'
            s.style.whiteSpace = 'normal'
            s.style.wordBreak = 'normal'
            s.style.wordSpacing = 'normal'
            s.innerHTML = testString
            return s
        }

        const createSpanWithFonts = function(fontToDetect, baseFont){
            const s = createSpan()
            s.style.fontFamily = `'${fontToDetect}',${baseFont}`
            return s
        }

        const initializeBaseFontsSpans = function(){
            const spans = []
            for (let idx=0, length=baseFonts.length; idx<length; idx++){
                let s = createSpan()
                s.style.fontFamily = baseFonts[idx]
                baseFontsDiv.appendChild(s)
                spans.push(s)
            }
            return spans
        }

        const initializeFontsSpans = function(){
            var spans = {}
            for (let i=0, l=fontList.length; i<l; i++){
                let fontSpans = []
                for (let j=0, numDFnts=baseFonts.length; j<numDFnts; j++){
                    let s = createSpanWithFonts(fontList[i], baseFonts[j])
                    fontsDiv.appendChild(s)
                    fontSpans.push(s)
                }
                spans[fontList[i]] = fontSpans
            }
            return spans
        }

        const isFontAvailable = function(fontSpans){
            let d = false
            for (let i=0; i<baseFonts.length; i++) {
                d = (fontSpans[i].offsetWidth !== defaultWidth[baseFonts[i]] || fontSpans[i].offsetHeight !== defaultHeight[baseFonts[i]])
                if(d){ return d }
            }
            return d
        }

        const baseFontsSpans = initializeBaseFontsSpans()
        h.appendChild(baseFontsDiv)

        for (let idx=0, length=baseFonts.length; idx<length; idx++){
            defaultWidth[baseFonts[idx]] = baseFontsSpans[idx].offsetWidth
            defaultHeight[baseFonts[idx]] = baseFontsSpans[idx].offsetHeight
        }

        const fontsSpans = initializeFontsSpans()
        h.appendChild(fontsDiv)

        const available = []
        for (let i=0, l=fontList.length; i<l; i++){
            if (isFontAvailable(fontsSpans[fontList[i]])) {
                available.push(fontList[i])
            }
        }
        return available
    }

}
