let response;
let mappo;
let table;
let latitude = -16.708112;
let longitude = -49.298196;
let latitude1;
let latitude2;
let longitude1;
let longitude2;
let zoom = 14;
let img;
let img2;
let point = [];
let startX;
let startY;
let factorX = 1;
let factorY = 1;
let result;
let nodes = [];
let nodeFactorX = 6;
let nodeFactorY = 6;
let nodeX = 4720;
let nodeY = 3600;
let mousePressedFlag = false;
let city = 0;
let id = 3462377;
let opacity = [255, 51, 51, 51, 51];
let cityName = "Goiânia";
let xArray = [{}];
let yArray = [{}];
let latAvg = 0;
let longAvg = 0;
let altFlag = false;
let moveX = 0;
let moveY = 0;

function preload() {

  table = loadTable('assets/20200218030824-95001-data.csv', 'csv', 'header', function (tab) {

    latitude1 = parseFloat(table.getString(1, 2));
    longitude1 = parseFloat(table.getString(1, 3));
    latitude2 = parseFloat(table.getString(1, 2));
    longitude2 = parseFloat(table.getString(1, 3));

    for (let r = 0; r < table.getRowCount(); r++) {

      if (parseFloat(table.getString(r, 2)) < latitude1) {
        latitude1 = parseFloat(table.getString(r, 2));
      }
      if (parseFloat(table.getString(r, 2)) > latitude2) {
        latitude2 = parseFloat(table.getString(r, 2));
      }
      if (parseFloat(table.getString(r, 3)) < longitude1) {
        longitude1 = parseFloat(table.getString(r, 3));
      }
      if (parseFloat(table.getString(r, 3)) > longitude2) {
        longitude2 = parseFloat(table.getString(r, 3));
      }
    }  
    print(latitude1,latitude2,longitude1,longitude2);
    latAvg = parseFloat(table.getString(1, 2));
    longAvg = parseFloat(table.getString(1, 3));
    mappo = loadImage('https://api.mapbox.com/styles/v1/ammarhsufyan/ck626h0280s471iqrah8m2zrl/static/' + longAvg + ',' + latAvg + ',' + zoom + '/900x600?access_token=pk.eyJ1IjoiY29tcmFkZXRvbnkiLCJhIjoiY2s1OGc0MHR5MG1pdTNrcXh4YmkxbWd1MSJ9.Pi057Qmj-o0xtJ1nUvCy8A');
   getNodes();
  });
  fontRegular = loadFont('assets/IBMPlexMono-Regular.ttf');
  
  // response = loadJSON('assets/response.json');  // For loading from local data
}


function setup() {
  canvas = createCanvas(windowWidth, windowHeight);


  getTemperature(tempCallback);
  updateMetrics();
  readTable();
  loopZooms();
  drawPath();
  drawPath();
  drawNodes();
  drawAltitude();
  drawTemperature(result.main.temp);
  drawCursor();
  updateMetrics();
  readTable();
  // generateNodes(); 
}


function draw() {
  background(153);
  image(mappo, 410, 200);

  // generateNodes();
  drawPath();
  drawNodes();
  drawAltitude();
  drawTemperature(result.main.temp);
  drawCursor();



}

function getTemperature(callback) {

  let _url = "https://api.openweathermap.org/data/2.5/weather?id=" + id + "&units=metric&APPID=ddb4a13e65527ca7aad155cb39bbd403";
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.onreadystatechange = function () {
    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
      callback(xmlHttp.responseText);
    }
  }
  xmlHttp.open("GET", _url, false); // true for asynchronous 
  xmlHttp.send(null);
}

function getNodes() {

  // let url = "https://api.wigle.net/api/v2/network/search?lastupdt=20191002&freenet=false&paynet=false&city=" + cityName;

  let url = "https://api.wigle.net/api/v2/network/search?onlymine=false&first=0&latrange1=" + latitude1 + "&latrange2=" + latitude2 + "&longrange1=" + longitude1 + "&longrange2=" + longitude2 + "&freenet=false&paynet=false"
  httpDo(
    url,
    {
      method: 'GET',
      // Other Request options, like special headers for apis
      headers: {
        accept: 'application/json',
        authorization: 'Basic QUlEM2I3MjhmYjQwMzQ0MGJhZTVhMWYzNmQxMGIxNjgwNjE6MTJhNjlmMTQ1NDdkMmFiZmYwOTNlOTJkNWVhZWE1NTg='
      }
    },
    function (res) {
      response = JSON.parse(res);
      console.log(response);
      if (response.message == "too many queries today")
        alert(response.message);
      generateNodes();
    }
  );

}


function tempCallback(resp) {
  result = JSON.parse(resp);
  drawTemperature(result.main.temp);
}

function loopZooms() {
  zoom = 13;
  for (var i = 13; i < 17; i++) {
    updateMetrics();
    readTable();
    generateArrays();
    zoom++;
  }
  zoom = 14;
}

function drawNodes() {
  if (city == 0) {
    for (let i = 0; i < nodes.length; i++) {
      nodes[i].display();

    }
  }
}

function drawCursor() {
  if (mouseX >= 410 && mouseY >= 200 && mouseX <= 1310 && mouseY <= 800) {
    //  Y line
    stroke(255, 255, 255);
    strokeWeight(1);
    line(mouseX, 0, mouseX, height);

    //  X line
    stroke(255, 255, 255);
    strokeWeight(1);
    line(0, mouseY, width, mouseY);

    cursor(CROSS);
  }
}

function drawPath() {

  if (city == 0) {
    let _x = startX - point[0].x - 1920;
    let _y = startY - point[0].y;
    if (zoom > 12) {
      for (index = 0; index < point.length - 1; ++index) {

        stroke(177, 243, 181);
        strokeWeight(zoom - 12);
        if (point[index].x + 1920 + _x + moveX >= 410 && point[index].y + _y + moveY >= 200 && point[index + 1].x + 1920 + _x + moveX <= 1310 && point[index + 1].y + _y + moveY <= 800) {
          line(point[index].x + 1920 + _x + moveX, point[index].y + _y + moveY, point[index + 1].x + 1920 + _x + moveX, point[index + 1].y + _y + moveY);

        }
      }
    }
  }
}
function generateArrays() {
  //min + Math.random() * (max - min)
  if (city == 0) {
    let _x = startX - point[0].x - 1920;
    let _y = startY - point[0].y;
    if (zoom > 12) {
      for (index = 0; index < point.length - 1; ++index) {
        // if (point[index].x + 1920 + _x >= 410 && point[index].y + _y >= 200 && point[index + 1].x + 1920 + _x <= 1310 && point[index + 1].y + _y <= 800) {
          xArray.push({ "zoom": zoom, "point": (point[index].x + 1920 + _x) + (-70 + Math.random() * (180 - 70)) });
          yArray.push({ "zoom": zoom, "point": (point[index].y + _y) + (-70 + Math.random() * (180 - 70)) });
        // }
      }
    }
  }
}


function generateNodes() {
  nodes = [];


  for (let i = 0; i < response.results.length; i++) {
    let r = int(Math.random()*xArray.length-1);
    if (xArray[r].zoom == zoom)
      nodes.push(new Node(xArray[r].point + response.results[i].qos * response.results[i].channel, yArray[r].point + response.results[i].qos * response.results[i].channel, response.results[i].qos, response.results[i].type, response.results[i]));

  }
}

function readTable() {

  point = [];
  latitude1 = parseFloat(table.getString(1, 2));
  longitude1 = parseFloat(table.getString(1, 3));
  latitude2 = parseFloat(table.getString(1, 2));
  longitude2 = parseFloat(table.getString(1, 3));

  //cycle through the table
  for (let r = 0; r < table.getRowCount(); r++) {

    if (parseFloat(table.getString(r, 2)) < latitude1) {
      latitude1 = parseFloat(table.getString(r, 2));
    }
    if (parseFloat(table.getString(r, 2)) > latitude2) {
      latitude2 = parseFloat(table.getString(r, 2));
    }
    if (parseFloat(table.getString(r, 3)) < longitude1) {
      longitude1 = parseFloat(table.getString(r, 3));
    }
    if (parseFloat(table.getString(r, 3)) > longitude2) {
      longitude2 = parseFloat(table.getString(r, 3));
    }

    append(point, { "x": getX(parseFloat(table.getString(r, 3))), "y": getY(parseFloat(table.getString(r, 2))) });

  }
  // print(latitude1, latitude2, longitude1, longitude2)
}
function drawAltitude() {
  let first = parseFloat(table.getString(1, 4));

  for (let r = 0; r < table.getRowCount(); r++) {

    paintAltitude(50, r * 3, (parseFloat(table.getString(r, 4)) - first - 40) * -4);

  }
}

function drawTemperature(temp) {

  textAlign(RIGHT);
  textSize(18);
  fill(177, 243, 181, opacity[0]);
  textFont(fontRegular);
  text("GOIÂNIA", 250, 100);

  // textSize(18);
  // fill(177, 243, 181, opacity[1]);
  // textFont(fontRegular);
  // text("RIO DE JANEIRO", 250, 120);

  // textSize(18);
  // fill(177, 243, 181, opacity[2]);
  // textFont(fontRegular);
  // text("SÃO PAULO", 250, 140);

  // textSize(18);
  // fill(177, 243, 181, opacity[3]);
  // textFont(fontRegular);
  // text("LISBOA", 250, 160);

  // textSize(18);
  // fill(177, 243, 181, opacity[4]);
  // textFont(fontRegular);
  // text("APARECIDA DE GOIÂNIA", 250, 180);

  let date = day() + "." + month() + "." + year();
  textSize(32);
  fill(177, 243, 181);
  textFont(fontRegular);
  text(round(latitude * 100) / 100, 500, 100);

  textSize(32);
  fill(177, 243, 181);
  textFont(fontRegular);
  text("/", 520, 100);

  textSize(32);
  fill(177, 243, 181);
  textFont(fontRegular);
  text(round(longitude * 100) / 100, 630, 100);

  textSize(20);
  fill(177, 243, 181);
  textFont(fontRegular);
  text(date, 500, 130);


  textSize(24);
  let colour = findTemp(temp);
  fill(colour);
  text(temp + "º", 1470, (50 - temp) * 20);


  paintTemperature();

}


function paintAltitude(_colour, _x, _y) {

  //pixels represented in an array manner; to find the pixel that corresponds to [x][y], multiply y*width for each column and add x
  //times 4 because it has four positions for each pixel, for RGBA
  let x = Math.round(_x);
  let y = Math.round(_y);

  x += 540;
  y += 720;

  let index = (x + y * width) * 4;

  let colour = findColour(parseInt(_y * 2) - 100);
  fill(colour);
  noStroke();
  // print(x, y)
  if (x < 1300 && y > 810) {

    ellipse(x, y, 8);
  }

}

function paintTemperature() {


  fill(226, 235, 52, 98);
  rect(1350, 200, 20, 400);

  fill(226, 235, 52, 98);
  rect(1350, 200, 20, 400);

  fill(52, 235, 177, 98);
  rect(1350, 300, 20, 400);

  fill(52, 235, 213, 98);
  rect(1350, 400, 20, 400);



}



//colours based on this table http://aqicn.org/api/
function findColour(colourNum) {
  if (colourNum < 0) {

    return color(220, 220, 220); // grey - no info

  }
  else if (colourNum <= 50) {

    return color(235, 201, 52); //orange
  }
  else if (colourNum <= 100) {
    return color(226, 235, 52); //gold

  }
  else if (colourNum <= 150) {
    return color(98, 235, 52); //parrot green
  }
  else if (colourNum <= 200) {

    return color(52, 235, 116); //green
  }
  else if (colourNum <= 300) {
    return color(52, 235, 177); //greenish
  }
  else if (colourNum <= 350) {
    return color(52, 235, 213); //cyan
  }
  else {

    return color(3, 194, 252); //blue
  }
}


function findTemp(colourNum) {

  if (colourNum < 0) {

    return color(220, 220, 220); // grey - no info

  }
  else if (colourNum <= 5) {

    return color(3, 194, 252); //blue

  }
  else if (colourNum <= 15) {

    return color(52, 235, 213); //cyan

  }
  else if (colourNum <= 25) {
    return color(52, 235, 177);

  }
  else if (colourNum <= 35) {

    return color(226, 235, 52); //gold
  }
  else if (colourNum <= 45) {
    return color(235, 201, 52); //orange
  }

  else {

    return color(245, 201, 52); //red
  }
}

function mouseWheel(event) {

  if (event.delta < 0 && zoom >= 14) {
    return;
  }
  else if (event.delta > 0 && zoom <= 10) {
    return;
  }
  if (event.delta > 0) {
    zoom--;
  }
  else if (event.delta < 0) {
    zoom++;
  }
  mappo = loadImage('https://api.mapbox.com/styles/v1/ammarhsufyan/ck626h0280s471iqrah8m2zrl/static/' + longAvg + ',' + latAvg + ',' + zoom + '/900x600?access_token=pk.eyJ1IjoiY29tcmFkZXRvbnkiLCJhIjoiY2s1OGc0MHR5MG1pdTNrcXh4YmkxbWd1MSJ9.Pi057Qmj-o0xtJ1nUvCy8A', function (imag) {

  });
  updateMetrics();
  readTable();
  generateNodes();
}





function updateCity() {
  switch (city) {
    case 0:
      id = 3462377;
      cityName = "Goiânia";
      latitude = -16.708112;
      longitude = -49.298196;
      break;

    case 1:
      id = 3451190;
      cityName = "Rio%20de%20Janeiro";
      latitude = -22.9121062;
      longitude = -43.2162545;
      break;

    case 2:
      id = 3448439;
      cityName = "SP";
      latitude = -23.6815315;
      longitude = -46.8754814;
      break;

    case 3:
      id = 2267057;
      cityName = "LSB";
      latitude = 38.7436057;
      longitude = -9.2302432;
      break;

    case 4:
      id = 3462377;
      cityName = "Aparecida%20de%20Goi%C3%A2nia";
      latitude = -16.7956377;
      longitude = -49.3147523;
      break;


  }


}


function updateMetrics() {
  switch (zoom) {

    case 10:
      nodeFactorX = 0;
      nodeFactorY = 0;
      nodeX = -1000;
      nodeY = -1000;
      break;

    case 11:
      nodeFactorX = 0.45;
      nodeFactorY = 0.45;
      nodeX = -520;
      nodeY = -250;
      break;
    case 12:
      nodeFactorX = 1;
      nodeFactorY = 1;
      nodeX = -110;
      nodeY = 60;
      break;
    case 13:
      startX = 856;
      startY = 495;
      factorX = 0.26;
      factorY = 0.26;
      nodeFactorX = 2;
      nodeFactorY = 2;
      nodeX = 635;
      nodeY = 600;
      break;

    case 14:
      startX = 856;
      startY = 490;
      factorX = 0.5;
      factorY = 0.5;
      nodeFactorX = 7.6;
      nodeFactorY = 7.6;
      nodeX = 4810;
      nodeY = 3700;
      break;

    case 15:
      startX = 856;
      startY = 480;
      factorX = 1;
      factorY = 1;
      nodeFactorX = 18;
      nodeFactorY = 18;
      nodeX = 12560;
      nodeY = 9450;
      break;

    case 16:
      startX = 850;
      startY = 466;
      factorX = 2;
      factorY = 2;
      nodeFactorX = 50;
      nodeFactorY = 50;
      nodeX = 36416;
      nodeY = 27144;
      break;

    case 17:
      startX = 845;
      startY = 435;
      factorX = 4;
      factorY = 4;
      nodeFactorX = 100;
      nodeFactorY = 100;
      nodeX = 73691;
      nodeY = 54808;
      break;

  }
}

function keyPressed() {
  if (keyCode === LEFT_ARROW) {
    moveX -= 232;
    longAvg += 0.01;
    mappo = loadImage('https://api.mapbox.com/styles/v1/ammarhsufyan/ck626h0280s471iqrah8m2zrl/static/' + longAvg + ',' + latAvg + ',' + zoom + '/900x600?access_token=pk.eyJ1IjoiY29tcmFkZXRvbnkiLCJhIjoiY2s1OGc0MHR5MG1pdTNrcXh4YmkxbWd1MSJ9.Pi057Qmj-o0xtJ1nUvCy8A');

  } else if (keyCode === RIGHT_ARROW) {
    moveX += 232;
    longAvg -= 0.01;
    mappo = loadImage('https://api.mapbox.com/styles/v1/ammarhsufyan/ck626h0280s471iqrah8m2zrl/static/' + longAvg + ',' + latAvg + ',' + zoom + '/900x600?access_token=pk.eyJ1IjoiY29tcmFkZXRvbnkiLCJhIjoiY2s1OGc0MHR5MG1pdTNrcXh4YmkxbWd1MSJ9.Pi057Qmj-o0xtJ1nUvCy8A');

  } else if (keyCode === UP_ARROW) {
    moveY -= 240;
    latAvg -= 0.01;
    mappo = loadImage('https://api.mapbox.com/styles/v1/ammarhsufyan/ck626h0280s471iqrah8m2zrl/static/' + longAvg + ',' + latAvg + ',' + zoom + '/900x600?access_token=pk.eyJ1IjoiY29tcmFkZXRvbnkiLCJhIjoiY2s1OGc0MHR5MG1pdTNrcXh4YmkxbWd1MSJ9.Pi057Qmj-o0xtJ1nUvCy8A');

  } else if (keyCode === DOWN_ARROW) {
    moveY += 240;
    latAvg += 0.01;
    mappo = loadImage('https://api.mapbox.com/styles/v1/ammarhsufyan/ck626h0280s471iqrah8m2zrl/static/' + longAvg + ',' + latAvg + ',' + zoom + '/900x600?access_token=pk.eyJ1IjoiY29tcmFkZXRvbnkiLCJhIjoiY2s1OGc0MHR5MG1pdTNrcXh4YmkxbWd1MSJ9.Pi057Qmj-o0xtJ1nUvCy8A');

  }
}


function getA(alt) {
  return ((((alt * 360) / 700) - 180) * 3.25) + 235;
}

function getNodeX(lon) {
  lon = ((((lon * 360) / 900) - 180) * 6) + 240;
  // print((((round(((lon + 880) * 1000) + 400) * 1.5) + 300) * -nodeFactorX) - nodeX);
  return (((round(((lon + 880) * 1000) + 400) * 1.5) + 300) * nodeFactorX) - nodeX;
}
//-22381821 2929835
function getNodeY(lat) {
  lat = ((90 - ((lat * 180) / 600)) * 6) - 240;
  // print((((round((lat - 388) * 1000) * 1.5) - 550) * nodeFactorY) - nodeY);
  return (((round((lat - 388) * 1000) * 1.5) - 550) * nodeFactorY) - nodeY;

}

function getX(lon) {
  lon = ((((lon * 360) / 900) - 180) * 6) + 240;
  return round((((lon) + 432.39) * 100000) / 2) * factorX;
}

function getY(lat) {
  lat = ((90 - ((lat * 180) / 600)) * 6) - 240;
  return round((((lat) - 330.07) * 100000) / 2) * factorY;
}


function getLat(y) {
  return (((-1 * y) + 90) * (mappo.height / 180));
}

function getLng(x) {
  return ((x + 180) * (mappo.width / 360));
}


function mouseClicked() {



  if (mouseY > 80 && mouseY < 100 && mouseX > 50 && mouseX < 250) {
    city = 0;
    updateCity();
    mappo = loadImage('https://api.mapbox.com/styles/v1/ammarhsufyan/ck626h0280s471iqrah8m2zrl/static/' + longitude + ',' + latitude + ',' + zoom + '/900x600?access_token=pk.eyJ1IjoiY29tcmFkZXRvbnkiLCJhIjoiY2s1OGc0MHR5MG1pdTNrcXh4YmkxbWd1MSJ9.Pi057Qmj-o0xtJ1nUvCy8A', function (imag) {
      updateMetrics();
      readTable();
      //generateNodes();

      for (let i = 0; i < opacity.length; i++) {
        opacity[i] = 51;
      }
      opacity[0] = 255;
      getTemperature(tempCallback);
    });
  }
  else if (mouseY > 100 && mouseY < 120 && mouseX > 50 && mouseX < 250) {
    city = 1;
    updateCity();
    mappo = loadImage('https://api.mapbox.com/styles/v1/ammarhsufyan/ck626h0280s471iqrah8m2zrl/static/' + longitude + ',' + latitude + ',' + zoom + '/900x600?access_token=pk.eyJ1IjoiY29tcmFkZXRvbnkiLCJhIjoiY2s1OGc0MHR5MG1pdTNrcXh4YmkxbWd1MSJ9.Pi057Qmj-o0xtJ1nUvCy8A', function (imag) {
      updateMetrics();
      readTable();
      //generateNodes();

      for (let i = 0; i < opacity.length; i++) {
        opacity[i] = 51;
      }
      opacity[1] = 255;
      getTemperature(tempCallback);
    });
  } else if (mouseY > 120 && mouseY < 140 && mouseX > 50 && mouseX < 250) {
    city = 2;
    updateCity();
    mappo = loadImage('https://api.mapbox.com/styles/v1/ammarhsufyan/ck626h0280s471iqrah8m2zrl/static/' + longitude + ',' + latitude + ',' + zoom + '/900x600?access_token=pk.eyJ1IjoiY29tcmFkZXRvbnkiLCJhIjoiY2s1OGc0MHR5MG1pdTNrcXh4YmkxbWd1MSJ9.Pi057Qmj-o0xtJ1nUvCy8A', function (imag) {
      updateMetrics();
      readTable();
      // generateNodes();

      for (let i = 0; i < opacity.length; i++) {
        opacity[i] = 51;
      }
      opacity[2] = 255;
      getTemperature(tempCallback);
    });
  } else if (mouseY > 140 && mouseY < 160 && mouseX > 50 && mouseX < 250) {
    city = 3;
    updateCity();
    mappo = loadImage('https://api.mapbox.com/styles/v1/ammarhsufyan/ck626h0280s471iqrah8m2zrl/static/' + longitude + ',' + latitude + ',' + zoom + '/900x600?access_token=pk.eyJ1IjoiY29tcmFkZXRvbnkiLCJhIjoiY2s1OGc0MHR5MG1pdTNrcXh4YmkxbWd1MSJ9.Pi057Qmj-o0xtJ1nUvCy8A', function (imag) {
      updateMetrics();
      readTable();
      // generateNodes();

      for (let i = 0; i < opacity.length; i++) {
        opacity[i] = 51;
      }
      opacity[3] = 255;
      getTemperature(tempCallback);
    });
  } else if (mouseY > 160 && mouseY < 180 && mouseX > 50 && mouseX < 250) {
    city = 4;
    updateCity();
    mappo = loadImage('https://api.mapbox.com/styles/v1/ammarhsufyan/ck626h0280s471iqrah8m2zrl/static/' + longitude + ',' + latitude + ',' + zoom + '/900x600?access_token=pk.eyJ1IjoiY29tcmFkZXRvbnkiLCJhIjoiY2s1OGc0MHR5MG1pdTNrcXh4YmkxbWd1MSJ9.Pi057Qmj-o0xtJ1nUvCy8A', function (imag) {
      updateMetrics();
      readTable();
      // generateNodes();

      for (let i = 0; i < opacity.length; i++) {
        opacity[i] = 51;
      }
      opacity[4] = 255;
      getTemperature(tempCallback);
    });
  }

}

function mousePressed() {

  mousePressedFlag = true;

}

function mouseReleased() {

  mousePressedFlag = false;

}

class Node {

  constructor(x, y, qos, type, node) {

    this.node = node;
    this.type = type;
    this.qos = qos;
    this.x = x;
    this.y = y;
    this.theta = 0;
    this.speed = 0.02;
    this.baseDiameter = 12;
    this.pulseAmplitude = 6+(zoom);

  }

  drawBar() {
    strokeCap(SQUARE);
    strokeWeight(25);
    if (this.type == "infra") {

      stroke(52, 235, 213);
    }
    else if (this.type == "ad-hoc") {

      stroke(255, 0, 0);
    }
    else {
      stroke(255, 192, 255);
    }

    line(moveX + this.x - (this.qos + 5 + ((zoom - 12) / 0.5)) / 2, 170,moveX +  this.x + (this.qos + 5 + ((zoom - 12) / 0.5)) / 2, 170);
    noStroke();
    strokeCap(ROUND);
  }

  drawDetails() {

    strokeWeight(2);
    stroke(255);
    line(mouseX, mouseY, 1380, 100);
    noStroke();
    textAlign(LEFT);
    textSize(16);
    fill(177, 243, 181);
    textFont(fontRegular);
    text(this.node.netid, 1400, 100);

    if (this.node.ssid != null) {
      textSize(16);
      fill(177, 243, 181);
      textFont(fontRegular);
      text(this.node.ssid, 1400, 120);
    }

    textSize(16);
    fill(177, 243, 181);
    textFont(fontRegular);
    text(this.node.encryption.toUpperCase(), 1400, 140);

    textSize(16);
    fill(177, 243, 181);
    textFont(fontRegular);
    text(this.node.lastupdt, 1400, 160);

    textSize(16);
    fill(177, 243, 181);
    textFont(fontRegular);
    text(this.node.channel, 1400, 180);

    textSize(16);
    fill(177, 243, 181);
    textFont(fontRegular);
    text(this.node.trilat, 1400, 200);

    textSize(16);
    fill(177, 243, 181);
    textFont(fontRegular);
    text(this.node.trilong, 1400, 220);

    textSize(16);
    fill(177, 243, 181);
    textFont(fontRegular);
    text(this.node.qos, 1400, 240);
    if (this.node.type != null) {
      textSize(16);
      fill(177, 243, 181);
      textFont(fontRegular);
      text(this.node.type, 1450, 240);
    }
    textAlign(RIGHT);
    noFill();
  }



  display() {

    if (mousePressedFlag) {
      if (moveX + this.x - (this.qos + 5 + ((zoom - 12) / 0.5)) / 2 <= mouseX && moveY + this.y + (this.qos + 5 + ((zoom - 12) / 0.5)) / 2 >= mouseY && moveX + this.x + (this.qos + 5 + ((zoom - 12) / 0.5)) / 2 >= mouseX && moveY +  this.y - (this.qos + 5 + ((zoom - 12) / 0.5)) / 2 <= mouseY) {
        this.drawDetails();
      }
    }

    if (this.x + moveX>= 410 && this.y + moveY>= 200 && this.x+ moveX <= 1310 && this.y+ moveY <= 800) {
      this.drawBar();

      var diam = this.baseDiameter + sin(this.theta * 4) * this.pulseAmplitude;

      ellipseMode(CENTER);
      if (this.qos > 2) {

        if (this.type == "infra") {

          fill(52, 235, 213, 50);
        }
        else if (this.type == "ad-hoc") {

          fill(255, 0, 0, 50);
        } else {
          fill(255, 192, 255,50);
        }

        
        ellipse(this.x + moveX, this.y + moveY, diam, diam);
        
        this.theta += this.speed;
      }

        if (this.type == "infra") {

          fill(52, 235, 213);
        }
        else if (this.type == "ad-hoc") {

          fill(255, 0, 0);
        }
        else {
          fill(255, 192, 255);
        }
        ellipseMode(CENTER);
        ellipse(this.x + moveX, this.y + moveY, this.qos + 5 + ((zoom - 12) / 0.5));


      
    }
  }
}
