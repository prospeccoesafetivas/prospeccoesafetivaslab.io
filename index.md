---
title: Home
layout: default
pagination: 
  enabled: true
---

<div class="container-fluid">
  <div class="row">
  {% for post in paginator.posts %}
    <div class="card  col-lg-4 col-xl-3">
      {% if post.thumbnail %}
        <img src="{{ post.thumbnail }}" /> 
      {% else %}
        <img src="" />
      {% endif %}
      <span>{{ post.artista }}</span>
      <p>
        <a style="text-decoration: none;" href="{{ BASE_PATH }}{{ post.url | remove: '/index.html' }}" class="shuf">
          {{ post.title }}
        </a>
      </p>
    </div>
  {% endfor %}
  </div>
  <div class="row">
    <div class="pagina col-sm-11">
      {% if paginator.page_trail %}
        {% for trail in paginator.page_trail %}
          <li {% if page.url == trail.path %}{% endif %}>
            <a href="{{ trail.path | prepend: site.baseurl }}" title="{{trail.title}}">{{ trail.num }}</a>.
          </li>
        {% endfor %}
      {% endif %}
    </div>
  </div>
</div>
