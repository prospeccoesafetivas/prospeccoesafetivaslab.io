---
layout: obra
title:  "@griteseries"
thumbnail: /assets/tumbnail/t-griteseries.png
artista: Raquel Kogan, Rejane Cantoni e AYO Cultural 
bio: https://www.cantoni-crescenti.com.br/
video-descricao:
texto-descricao: O @griteseries deriva de especulações sobre possibilidades narrativas de grandes coleções de mini-performances veiculadas em mídia social. O resultado, que a principio conecta estranhos através de suas performances, visa elaborar um novo modo de colaboração e produção criativa em escala universal; um mega-documentário realizado por centenas ou milhares de participantes, sem roteiro e sem diretor.Gritos podem ser realizados por qualquer criatura que possua pulmões, incluindo humanos. Uma obra composta de contribuições de milhões de pessoas pode demonstrar que nenhum GRITO soa como outro; à medida que as contribuições preenchem a tela, diferenças sonoras, visuais e culturais se tornam evidentes.@griteseries foi concebido durante o isolamento físico e social, para Instagram. Está desenhado para expandir em escala global, propagar em múltiplas plataformas, assumir diferentes renderings e formatos de exibição.
ano: 2020
---
<a href="https://www.instagram.com/griteseries/?hl=en" target="_blank">
<img src="/assets/griteseries/griteseries.png" class="img-fluid" alt="PoemAPP"/>
</a>
<br>

`Projeto idealizado por Raquel Kogan, Rejane Cantoni e AYO Cultural` 
<p><a href="https://www.instagram.com/griteseries/?hl=en" target="_blank">@griteseries</a> é uma chamada aberta à todos os humanos de qualquer lugar do
planeta.</p>
<p>A proposta é simples: GRITE todas as vezes que não puder se calar!
Para participar, é necessário seguir as seguintes etapas:</p>

1. grite;
2. registre seu grito;
4. faça o upload do resultado em um dos perfis de mídia social do projeto
(i.e., twitter, instagram, facebook).

>Os arquivos enviados são compilados por software customizado e exibidos no perfil Instagram do projeto.

