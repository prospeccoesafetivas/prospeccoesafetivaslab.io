---
layout: obra
title:  "Meta Image 01"
thumbnail: /assets/tumbnail/t-meta-image-01.png
artista: Marck Al
bio: http://marckal.com/about/
video-descricao: 
texto-descricao: Meta Imagem 01 (MI—01) é o primeiro projeto de uma série de trabalhos dedicados em visualizar a relação entre os dados e o espaço urbano. Nele a atividade da redes sem fio são mapeadas e co-relacionadas com a atuação individual no espaço urbano afim de discutir noção de atividade de dados, privacidade e distribuição da infraestrutura de informação e comunicação das cidades.
ano: 2019
---
<a href="http://metaimage01.marckal.com/" target="_blank">
<img src="/assets/meta-image.png" class="img-fluid" alt="Meta Image 01"/> </a>
<br>

>Work in Progress

`A` <a href="http://metaimage01.marckal.com/" target="_blank"> `versão interativa` </a> `pode não ser compatível com resoluções abaixo de 1920 X 1080.`  