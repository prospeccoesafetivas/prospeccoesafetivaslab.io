---
layout: obra
title:  "Later Date"
thumbnail: /assets/tumbnail/t-later-date.png
artista: Lauren Lee McCarthy
bio: http://lauren-mccarthy.com/
video-descricao: 
texto-descricao: I think one day we will be able to go outside again. Honestly, I am fantasizing about this day. Seeing you. Reaching out and touching. Breathing, talking, anything really. This is a performance in two parts. In the first, we will chat. We will imagine together our meeting. Where we’ll go, what we’ll say, what we’ll do. This future script will be saved. One day, when we are allowed again, you will receive an email with this script and a request to meet as planned. This will be part two of the performance.
ano: 2020
---

<iframe class="frame" src="https://laterdate.net/plans/"></iframe>

