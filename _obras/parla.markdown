---
layout: obra
title:  "Parla"
thumbnail: /assets/tumbnail/t-parla.png
artista: Guto Nóbrega
bio: https://gutonobrega.co.uk/
video-descricao: 
texto-descricao: Parla is an interactive system for the “animation” of a virtual body. The virtual body responds to levels of sound energy captured by a microphone. An algorithm analyses the sound input as dynamic variables for the creation of a digital entity. The virtual body is composed of a group of twelve body parts vertically organized in order to create an individual. Each part belongs to the body of different people stored in the system’s memory. This individual, found in a state of metaestability, will change its form/identity by swapping its parts in response to the level of sound produced by a viewer or another audible stimulus present in its milieu. This artwork was though as a net-art, as the cyberspace seems to be the best place to this virtual entity to live and change its multiple identity. This body is not thought as an ideal one but a possible body. Although it keeps in its silicon memory records of its physical identity, a group of young students, it is from the dialogue between the viewer and the virtual body which emerges a new individual.
ano: 2009
---
`ATENÇÃO ESSA OBRA USA TECNOLIGIA FLASH`
<br>
`Para saber como habilitar o flash acesse os respectivos links`
<br>
<a href="https://support.google.com/chrome/answer/6258784?co=GENIE.Platform%3DDesktop&hl=pt-BR" target="_blank">`CHROME` </a> <a href="https://support.mozilla.org/pt-BR/kb/plugin-flash-mantenha-o-atualizado-evite-problemas#w_avisos-para-executar-adobe-flash" target="_blank">`FIREFOX` </a> 


<br>
<br>
<embed src="/assets/parla.swf" width="1080" height="720">
<br>
<br>


