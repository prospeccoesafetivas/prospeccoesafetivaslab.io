---
layout: obra
title:  "lamentos_urbanos"
thumbnail: /assets/tumbnail/t-lamentos-urbanos.png
artista: Artur Cabral
bio: https://arturcabral.me/
video-descricao: 
texto-descricao: Lamentos_urbanos se estrutura no âmbito de reflexões que emergem neste momento de pandemia. A obra traz uma singular visualização e sonorização dos dados referente à quantidade de infectados e óbitos relacionados ao novo coronavírus nos municípios brasileiros. Essa aplicação atualizada em tempo real é uma investigação do sensível, como resposta perante o momento atual, que nos coloca num conjunto de medo e  incertezas. E não seria possível sem os dados de boletins informativos e casos do coronavírus por município diariamente publicado e revisado por Álvaro Justen e dezenas de colaboradores (brasil.io).
ano: 2020
---

<iframe class="frame" src="https://arturcabral.me/lamentos-urbanos/"></iframe>

