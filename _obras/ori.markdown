---
layout: obra
title:  "Ori.0.0.6"
thumbnail: /assets/tumbnail/t-ori.png
artista: Teófilo Augusto
bio: https://www.teoaugusto.com.br/
video-descricao: 
texto-descricao: O Origami é uma técnica milenar japonesa de dobradura de papel que se difundiu no mundo todo. Por meio de técnicas como vincos, cortes e dobras internas e externas pequenos bichos, utensílios e até figuras mitológicas surgem. Então como seria se essas dobras tivessem movimento ligados aos movimentos do corpo humano? Assim, pontos de origamis são ligados à membros humanos reconhecidos por sensores de movimento. Neste ponto do desenvolvimento ainda estão sendo exploradas as técnicas de animação que dariam a movimentação de um objeto feito de papel e no mundo virtual.
ano: 2020
---

<video width="952" height="535.5" autoplay loop>
  <source src="/assets/ori/ori.webm" type="video/webm"  />
  <source src="/assets/ori/ori.mp4" type="video/mp4"  />
</video>