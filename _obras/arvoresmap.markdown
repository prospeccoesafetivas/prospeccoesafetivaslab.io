---
layout: obra
title:  "ArvoresMaps"
thumbnail: /assets/tumbnail/t-arvoresmaps.png
artista: Suzete Venturelli
bio: https://icon.ufba.br/
video-descricao: 
texto-descricao: Árvore Mapa. A arte generativa não é programação nem arte. É ambas e nenhuma dessas coisas. A programação é uma interface entre humano, natureza e máquina; Nessa obra a proposta  apresenta um sujeito emocional, definição altamente subjetiva e desafiadora. Arte generativa é o ponto de encontro entre  a ideia de se apropriar de processos matemáticos e subvertê-los para criar resultados ilógicos, imprevisíveis e expressivos. Arte generativa é como uma árvore; mas suas sementes programadas e eletrônicas, em vez de solo e água. É uma propriedade emergente do mais simples dos processos&#58; Decisões algorítmicas. A arte generativa nesse trabalho apresenta o orgânico computacional.
ano: 2020
---

<iframe class="frame" src="https://www.openprocessing.org/sketch/873434/embed/"></iframe>


