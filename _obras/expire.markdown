---
layout: obra
title:  "Expire"
thumbnail: /assets/tumbnail/t-expire.png
artista: Ive Rubini
bio: https://iverubini.hotglue.me/
video-descricao:
texto-descricao: Expire é um código em JavaScript de comandos básicos que permite registrar frases, reflexões, cantorias e movimentos respiratórios em formas geométricas abstratas. Para interagir, basta falar ao microfone do computador. Com o clique do mouse também são geradas formas moduladas pela emissão de sons. O trabalho discute formas alternativas de expressão verbal, tornando a comunicação sonora uma ferramenta de criação artística visual. Foi feito a partir de práticas coletivas com o grupo Programação Recreativa, mediado pelo artista Diego de los Campos durante a quarentena do COVID-19.
ano: 2020
---

<iframe class="frame" src="/obras-externas/expire/"></iframe>
