---
layout: obra
title:  "Your digital fingerprint"
thumbnail: /assets/tumbnail/t-your-digital-fingerprint.png
artista: Nick Briz
bio: http://nickbriz.com/
video-descricao:
texto-descricao: Your digital fingerprint is made up of tiny bits of your personal data. This distinct, data-driven identifier is currently in the possession of countless corporate entities. The artwork above is a visual representation of your unique fingerprint, and it uses the same data as those corporations. It was created to serve as a reminder that we’re never truly anonymous online and, just as you have a right to privacy, you have a right to know when and how that privacy is being violated. Understanding how specific tracking methods work should not be the exclusive domain of those who seek to capitalize on your web activity. This work gives visibility to a tracking technique widely used, but rarely discussed browser fingerprinting. (BRIZ, N., & D'ARNAULT, C. (2018).This is Your Digital Fingerprint, n. 2, in thedisconnect.co)
ano: 2018
---

<iframe class="frame" src="/obras-externas/your-digital-fingerprint/"></iframe>
