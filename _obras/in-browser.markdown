---
layout: obra
title:  "&#95;iN_bROWSERsTAYS iN bROWSER"
thumbnail: /assets/tumbnail/t-in-browser.png
artista: Zsolt Mesterhazy
bio: 
video-descricao: 
texto-descricao: The piece dictates a pace by asking for a password. Would the mention of popular and common be a hint at what that is? By implication, the same dilemma seasons the debate between the two Pikachus in the background. Only when the cryptographer enters the right guess does the page move on. - But let me stop here to point out that the browser does all crosschecks locally, nothing entered gets sent anywhere. - To reaffirm having guessed the password a “mild surprise” kaomoji appears and if clicked on it turns into a “shy” kaomoji and the page goes on mumbling about the difference between the two adjectives. After which some otherwise interesting aspects get dismissed only to go on a sidetrack of a device related story by showing a qr code card. As one scrolls on, a picture presents two men on Ipanema beach, they are a street seller and an officer of deck chairs and parasols dressed in a uniform advertising fruit juice. The two men are clearly engaged in an exchange about the qr code cards the street seller has at that moment.
ano: 2020
---

<iframe class="frame" src="/obras-externas/in-browser/"></iframe>

