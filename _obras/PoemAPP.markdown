---
layout: obra
title:  "PoemAPP Covid-19"
thumbnail: /assets/tumbnail/t-poemapp.png
artista: LabFront
bio: https://labfront.weebly.com/
video-descricao: 
texto-descricao: O PoemAPP "Covid-19" foi desenvolvido por Pablo Gobira e Antônio Mozelli, membros do [Laboratório de Poéticas Fronteiriças](http://labfront.tk).
ano: 2020
---

<img src="/assets/poemapp/covid.png" class="img-fluid" alt="PoemAPP"/>
<br>

<p>Este é o PoemAPP "Covid-19", um poema digital em realidade aumentada. Instalando o aplicativo em seu smartphone com sistema operacional Android, você poderá ver e interagir com o poema. Após instalar o aplicativo: </p>

<a href="https://drive.google.com/file/d/11G1-fl87TF1cV-qYS8P7PhdAi4F9wdks/view?usp=sharing"> LINK PARA APK </a> `Compatível com Android Lollipop 5.1 ou superior` 

1. abra a <a href="/assets/poemapp/imagem.pdf">imagem </a> do vírus que causa o Covid-19 em uma tela para a qual possa apontar a câmera do seu smartphone;
2. abra o aplicativo Covid19 em seu smartphone e aponte a câmera traseira para a imagem do vírus;
3. caso deseje interaja com o aplicativo enquanto as palavras do saem do vírus.

O PoemAPP "Covid-19" foi desenvolvido por Pablo Gobira e Antônio Mozelli, membros do [Laboratório de Poéticas Fronteiriças](http://labfront.tk).


>Pablo Gobira é professor da Escola Guignard (UEMG), do PPGArtes (UEMG) e do PPGGOC (UFMG). Membro pesquisador e gestor de serviços da Rede Brasileira de Serviços de Preservação Digital (IBICT/MCTIC). Coordenador do grupo de pesquisa (CNPq) Laboratório de Poéticas Fronteiriças [http://labfront.tk]. Escritor e editor dos livros: "A memória do digital e outras questões das artes e museologia" (EdUEMG, 2019), "Percursos contemporâneos: realidades da arte, ciência e tecnologia" (EdUEMG, 2018) dentre outros livros e artigos. Atua: na curadoria, criação e produção no campo da cultura, artes digitais e ciências; como professor em cursos de fronteira como o Curso de Engenharia de Máquinas Biológicas (UFMG, UEMG, UFV e Newton Paiva); curadoria de bienal, exposições e residências artísticas. É coordenador do Programa Institucional de Extensão (UEMG) Direitos à Produção e ao Acesso à Arte e à Cultura.

>Antônio Mozelli é mestre em Artes (UEMG). Possui graduação em Ciência da Computação pela Universidade FUMEC e graduação no Bacharelado em Artes Plásticas na Escola Guignard, UEMG. Atualmente desenvolve pesquisa e trabalhos no campo das artes digitais e explora a utilização de realidade virtual em ambientes de imersão interativa e inteligência artificial. É integrante do grupo de pesquisa (CNPq) Laboratório de Poéticas Fronteiriças [LabFront].