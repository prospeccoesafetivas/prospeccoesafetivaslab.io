// BLOTTER  - Afetivas 
var text2 = new Blotter.Text("PROSPECÇÕES", {
  family : "'EB Garamond', serif",
  size : 27,
  fill : "#fff",
  paddingLeft : 10,
  paddingRight : 10,
  width: 1
});


var material = new Blotter.LiquidDistortMaterial();

material.uniforms.uSpeed.value = 0.2;

material.uniforms.uVolatility.value = 0.07;

material.uniforms.uSeed.value = 2.5;


var blotter = new Blotter(material, {
  texts : text2

});

var elem = document.getElementById("prospeccoes");
var scope = blotter.forText(text2);

scope.appendTo(elem);

// BLOTTER  - Prospecções 
var text = new Blotter.Text("AFETIVAS", {
  family : "'EB Garamond', serif",
  size : 27,
  fill : "#fff",
  paddingLeft : 10,
  paddingRight : 10

});


var material = new Blotter.LiquidDistortMaterial();

material.uniforms.uSpeed.value = 0.2;

material.uniforms.uVolatility.value = 0.07;

material.uniforms.uSeed.value = 2;

var blotter = new Blotter(material, {
  texts : text

});

var elem = document.getElementById("afetivas");
var scope = blotter.forText(text);

scope.appendTo(elem);
