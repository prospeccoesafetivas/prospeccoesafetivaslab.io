---
title: Ficha Técnica
layout: default
---

<div class="container-fluid">
  <div class="row">
<div class="ficha-tecnica col-sm-10">
  <h1>Ficha Técnica</h1>
  <p><b><a href="https://docs.google.com/spreadsheets/d/1URoqG4bmROXA86suNN9zWLzQHturS9bAjAIymNqmDrw/edit?usp=sharing" target="_blank">Livro de assinaturas / Guest book</a></b></p>
  <p><b>Produção e Curadoria:</b> Artur Cabral</p>
  <p><b>Assistente de produção técnica:</b> Joenio Costa</p>
  <p><b>Assistentes de produção executiva:</b> Tainá Luize, Nycacia Delmondes, Suzete Venturelli e Antenor Ferreira</p>
  <p><b><a href="mailto:medialabunbdf@gmail.com">Fale com a gente ;)</a></b></p>
  <p>
    <b>Artistas:</b><br>
    <span>
      Alexandre Rangel (DF) 🇧🇷 <br>
      André Gomes Bahiense (BA) 🇧🇷 <br>
      Antônio Mozelli (BR / CA) 🇧🇷 <br>
      Artur Cabral (DF) 🇧🇷 <br>
      Domenico Barra (IT) 🇮🇹 <br>
      Fellipe Narde Oliveira da Hora (BA) 🇧🇷 <br>
      Francisco de Paula Barretto (BA) 🇧🇷 <br>
      Guto Nóbrega (RJ) 🇧🇷 <br>
      Hamilton Greene (USA) 🇺🇸 <br>
      Lauren Lee McCarthy (USA) 🇺🇸 <br>
      Nick Briz (USA) 🇺🇸 <br>
      Marck Al (GO) 🇧🇷 <br>
      Pablo Gobira (MG) 🇧🇷 <br>
      Ramon Lago Alves Freire (BA) 🇧🇷 <br>
      Raquel Kogan (SP) 🇧🇷 <br>
      Rejane Cantoni (SP) 🇧🇷 <br>
      Suzete Venturelli (DF/SP) 🇧🇷 <br>
      Teófilo Augusto (PA) 🇧🇷 <br>
      Ive Rubini (SP) 🇧🇷 <br>
      Victor Calazans Ramos (BA) 🇧🇷 <br>
      Zsolt Mesterhazy (NL) 🇳🇱 <br>
    </span>
  </p>
  <p>
    <b>Performances de Abertura:</b><br>
    <i>(16 de Maio de 2020)</i><br>
    <span>
      <a href="/abertura"> Veja como foi nossa VERNISSAGE :)</a><br>
      19:00 <a href="https://gilfuser.net">Gil Fuser</a> (SP)<br>
      19:20 <a href="https://pietrobapthysthe.bandcamp.com">Pietro Bapthysthe</a> (PE)<br>
      19:40 <a href="http://www.alexandrerangel.art.br">Alexandre Rangel</a> (DF)<br>
      20:00 <a href="https://colectivo-de-livecoders.gitlab.io">IG noto</a> (AR, IN, CO, ID, BR)<br>
      20:20 <a href="https://ghales.top">Thales Grilo</a> (DF)<br>
      20:40 <a href="http://hitnail.net">Beise</a> (MG)
    </span>
  </p>
</div>
</div>
</div>


